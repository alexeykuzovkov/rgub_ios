//
//  Hall.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Hall : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *contacts;
@property (nonatomic, strong) UIImage *image;
@end
