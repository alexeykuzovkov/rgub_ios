//
//  LKViewController.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LKModel.h"
#import "UserInfo.h"
#import "BookOnHands.h"

@interface LKViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ModelDelegate>
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *ticketNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *endDateLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)prolongTicket:(id)sender;
- (IBAction)logout:(id)sender;

@end
