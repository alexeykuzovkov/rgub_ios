//
//  UserInfo.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "Model.h"
#import <UIKit/UIKit.h>

@interface UserInfo : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *ticketNumber;
@property (nonatomic, strong) NSDate *ticketEndDate;
@property (nonatomic, strong) UIImage *avatar;

@end
