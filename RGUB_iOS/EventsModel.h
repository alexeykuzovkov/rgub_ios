//
//  EventsModel.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model.h"
#import "Event.h"

@interface EventsModel : Model

+ (id)sharedModel;

@property (nonatomic, readonly, strong) NSArray *events;

@end
