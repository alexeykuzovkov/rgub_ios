//
//  ContactsModel.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "ContactsModel.h"

@implementation ContactsModel
+ (id)sharedModel {
    static ContactsModel *sharedModel = nil;
    @synchronized(self) {
        if (sharedModel == nil) {
            sharedModel = [[self alloc] init];
        }
    }
    return sharedModel;
}

-(id)init {
    if(self==[super init]) {
        
        Contact *c1 = [[Contact alloc] init];
        c1.name = @"Справочная служба";
        c1.contacts = @[@"+7 499 922-66-77"];
        
        Contact *c2 = [[Contact alloc] init];
        c2.name = @"Сектор записи читателей, информации и статистики";
        c2.contacts = @[@"+7 499 922-66-77", @"register@rgub.ru"];
        
        _contacts = @[c1, c2];
    }
    return self;
}
@end
