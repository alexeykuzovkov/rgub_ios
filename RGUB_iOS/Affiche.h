//
//  Affiche.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Affiche : NSObject
@property(nonatomic, strong) NSString *date;
@property(nonatomic, strong) NSString *time;
@property(nonatomic, strong) NSString *location;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *subtitle;

@end
