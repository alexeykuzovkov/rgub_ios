//
//  BooksSearchResultsTableViewController.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "BooksSearchResultsTableViewController.h"

@interface BooksSearchResultsTableViewController ()
{
    SearchModel *searchModel;
    LeaseModel *leaseModel;
}
@end

@implementation BooksSearchResultsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    
    searchModel = [[SearchModel alloc] initWithDelegate:self];
    leaseModel = [[LeaseModel alloc] init];
    [leaseModel setDelegate:self];
    
    switch ((int)_searchType) {
        case Author:
            [searchModel searchByAuthor:_searchQuery];
            break;
        case Title:
            [searchModel searchByTitle:_searchQuery];
            break;
        case ISBN:
            [searchModel searchByISBN:_searchQuery];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[searchModel searchResults] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"SearchBooksCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSLog(@"searchResults %@",[searchModel searchResults]);
    
    SearchResultBook *book = [[searchModel searchResults] objectAtIndex:indexPath.row];
    
    [(UILabel*)[cell.contentView viewWithTag:1] setText:book.title];
    [(UILabel*)[cell.contentView viewWithTag:2] setText:book.author];
    [(UILabel*)[cell.contentView viewWithTag:3] setText:book.isbn];
    [(UIImageView*)[cell.contentView viewWithTag:4] setImage:book.cover];
    UIButton *cellButton = (UIButton*)[cell.contentView viewWithTag:5];
    [cellButton addTarget:self action:@selector(lease:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchResultBook *book = [[searchModel searchResults] objectAtIndex:indexPath.row];
    [leaseModel leaseBookByIsbn:book.isbn];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}
#pragma mark ModelDelegate
-(void)modelDidLoadDataFromRemoteSource:(Model*)model {
    if (model==searchModel) {
        [self.tableView reloadData];
    }
    if (model==leaseModel) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Успешно"
                                                                                  message: @"Книга успешно забронирована"
                                                                           preferredStyle:UIAlertControllerStyleAlert];

        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
-(void)modelDidLoadSavedData:(Model*)model {
    
}
-(void)modelDidFailToLoadDataFromRemoteSource:(Model*)model {
    
}
-(void)modelHasNoSavedData:(Model*)model {
    
}

@end
