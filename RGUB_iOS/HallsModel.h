//
//  HallsModel.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "Model.h"
#import "Hall.h"
@interface HallsModel : Model

+ (id)sharedModel;

@property (nonatomic, strong, readonly) NSArray *halls;

@end
