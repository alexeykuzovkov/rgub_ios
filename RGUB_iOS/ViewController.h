//
//  ViewController.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MTBBarcodeScanner.h"
#import "BooksSearchResultsTableViewController.h"
#import "LKModel.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, ModelDelegate>

@property (strong, nonatomic) IBOutlet UITextField *searchField;
@property (strong, nonatomic) IBOutlet UITableView *eventsTableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *searchContextSegmentedControl;

- (IBAction)unwindToMainMenu:(UIStoryboardSegue*)sender;

- (IBAction)scanISBN:(id)sender;
@end

