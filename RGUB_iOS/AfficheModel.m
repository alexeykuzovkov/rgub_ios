//
//  AfficheModel.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "AfficheModel.h"

@implementation AfficheModel
+ (id)sharedModel {
    static AfficheModel *sharedModel = nil;
    @synchronized(self) {
        if (sharedModel == nil) {
            sharedModel = [[self alloc] init];
        }
    }
    return sharedModel;
}

-(id)init {
    if(self==[super init]) {
        Affiche *aff1 = [[Affiche alloc] init];
        aff1.date = @"22/04";
        aff1.time = @"10:30";
        aff1.title = @"Хакатон";
        aff1.subtitle = @"Форум разработчиков, участники которого создадут мобильное приложение для РГБМ. Авторы лучшего проекта получат ценные призы";
        aff1.location = @"Комикс-центр";
        
        Affiche *aff2 = [[Affiche alloc] init];
        aff2.date = @"24/04";
        aff2.time = @"15:30";
        aff2.title = @"Весна в русской поэзии Золотого века";
        aff2.subtitle = @"Литературно-музыкальный вечер, посвящённый Аполлону и Владимиру Григорьевым";
        aff2.location = @"Особняк Носова";
        
        _affiche = @[aff1, aff2];
    }
    return self;
}
@end
