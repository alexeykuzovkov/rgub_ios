//
//  HallsModel.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "HallsModel.h"

@implementation HallsModel

+ (id)sharedModel {
    static HallsModel *sharedModel = nil;
    @synchronized(self) {
        if (sharedModel == nil) {
            sharedModel = [[self alloc] init];
        }
    }
    return sharedModel;
}

-(id)init {
    if(self==[super init]) {
        
        Hall *h1 = [[Hall alloc] init];
        h1.name = @"Зал музыкальный подвал";
        h1.contacts = @[@"+7 499 922-66-77"];
        h1.image = [UIImage imageNamed:@"logo.png"];
        
        Hall *h2 = [[Hall alloc] init];
        h2.name = @"Музей электронной книги";
        h2.contacts = @[@"+7 499 922-66-77", @"register@rgub.ru"];
        h2.image = [UIImage imageNamed:@"logo.png"];
        
        _halls = @[h1, h2];
    }
    return self;
}

@end
