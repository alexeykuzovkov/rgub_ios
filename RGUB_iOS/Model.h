//
//  Model.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchConnection.h"

#define kServerAdress @"http://62.213.67.145:3000"

@class Model;
@protocol ModelDelegate <NSObject>
-(void)modelDidLoadDataFromRemoteSource:(Model*)model;
-(void)modelDidLoadSavedData:(Model*)model;
-(void)modelDidFailToLoadDataFromRemoteSource:(Model*)model;
-(void)modelHasNoSavedData:(Model*)model;
@end

@interface Model : NSObject 

@property (nonatomic, assign) id <ModelDelegate> delegate;

@end
