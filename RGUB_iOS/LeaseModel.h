//
//  LeaseConnection.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKModel.h"
@interface LeaseModel : Model <SearchConnectionDelegate>
-(void)leaseBookByIsbn:(NSString*)isbn;
@end
