//
//  SearchModel.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "SearchModel.h"

@implementation SearchModel
-(id)initWithDelegate:(id)delegate {
    if (self == [super init]) {
        self.delegate = delegate;
        connection = [[SearchConnection alloc] init];
        [connection setDelegate:self];
    }
    return self;
}

-(void)setTestSearchResults {
    SearchResultBook *book1 = [[SearchResultBook alloc] init];
    [book1 setTitle:@"Книга 1"];
    [book1 setAuthor:@"Автор 1"];
    [book1 setIsbn:@"1123123123123123"];
    [book1 setCover:[UIImage imageNamed:@"logo.png"]];
    
    SearchResultBook *book2 = [[SearchResultBook alloc] init];
    [book2 setTitle:@"Книга 2"];
    [book2 setAuthor:@"Автор 2"];
    [book2 setIsbn:@"2222222"];
    [book2 setCover:[UIImage imageNamed:@"logo.png"]];
    
    
    _searchResults = @[book1, book2];
}

-(void)searchByAuthor:(NSString*)author {
    [connection connect:[NSString stringWithFormat:@"%@/author/%@",kServerAdress, author]];
}
-(void)searchByTitle:(NSString*)title {
    [connection connect:[NSString stringWithFormat:@"%@/title/%@",kServerAdress, title]];
    
}
-(void)searchByISBN:(NSString*)isbn {
    [connection connect:[NSString stringWithFormat:@"%@/isbn/%@",kServerAdress, isbn]];
}

#pragma mark connection
-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    NSLog(@"%@",[loadedData objectForKey:@"data"]);
    
    NSMutableArray *loadedBooks = [[NSMutableArray alloc] init];
    for (NSDictionary *obj in [loadedData objectForKey:@"data"]) {
        SearchResultBook *book1 = [[SearchResultBook alloc] init];
        [book1 setTitle:[obj objectForKey:@"title"]];
        [book1 setAuthor:[obj objectForKey:@"name"]];
        [book1 setIsbn:[obj objectForKey:@"isbn"]];
        [book1 setCover:[UIImage imageNamed:[obj objectForKey:@"pic"]]];
        [loadedBooks addObject:book1];
    }
    
    _searchResults = [NSArray arrayWithArray:loadedBooks];
    
    [self.delegate modelDidLoadDataFromRemoteSource:self];
}

-(void)searchConnectionErrorWithText:(NSString *)error {
    
}
@end
