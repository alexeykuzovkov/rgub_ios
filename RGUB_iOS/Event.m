//
//  Event.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "Event.h"

@implementation Event
+ (instancetype)eventWithTitle: (NSString *)title andLink:(NSString*)link {
    return [[self alloc] initWithTitle:title andLink:link];
}

-(id)initWithTitle:(NSString*)title andLink:(NSString*)link {
    if (self = [super init]) {
        self.title = title;
        self.link = link;
    }
    return self;
}

@end
