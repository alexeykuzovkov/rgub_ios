//
//  LKModel.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "LKModel.h"

@implementation LKModel

+ (id)sharedModel {
    static LKModel *sharedModel = nil;
    @synchronized(self) {
        if (sharedModel == nil) {
            sharedModel = [[self alloc] init];
        }
    }
    return sharedModel;
}
-(BOOL)isLogined {
//    NSString *userToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"usertoken"];
    BOOL logined = [[NSUserDefaults standardUserDefaults] boolForKey:@"logined"];
    return logined;
}

-(void)loginWithCardNumber:(NSString*)cardNumber {
    [connection connect:[NSString stringWithFormat:@"%@/sign/%@",kServerAdress, cardNumber]];
}

-(id)init {
    if(self==[super init]) {
//        _userInfo = [[UserInfo alloc] init];
//        [_userInfo setName:@"Толик 2 Местный Алкоголик"];
//        [_userInfo setAvatar:[UIImage imageNamed:@"Tl_an.png"]];
//        [_userInfo setTicketNumber:@"123123"];
//        [_userInfo setTicketEndDate:[NSDate dateWithTimeIntervalSinceNow:60*60*24*3]];
        
//        BookOnHands *book1 = [[BookOnHands alloc] init];
//        book1.title = @"Про федота стрельца";
//        book1.author = @"Леонид Филатов";
//        book1.leaseEndDate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*2];
//        book1.cover = [UIImage imageNamed:@"logo.png"];
//        
//        BookOnHands *book2 = [[BookOnHands alloc] init];
//        book2.title = @"Совершенный код";
//        book2.author = @"Стив Макконнелл";
//        book2.leaseEndDate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*2];
//        book2.cover = [UIImage imageNamed:@"logo.png"];
//        
//        _booksOnHands = @[book1, book2];
        
        connection = [[SearchConnection alloc] init];
        [connection setDelegate:self];
    }
    return self;
}

#pragma mark connection
-(void)loadRemoteProfile {
    SearchConnection *_connection = [[SearchConnection alloc] init];
    [_connection setDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"%@/profile/%@",kServerAdress, [[NSUserDefaults standardUserDefaults] stringForKey:@"usertoken"]]];
}

-(void)loadLeaseInfo {
    SearchConnection *_connection = [[SearchConnection alloc] init];
    [_connection setDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"%@/lease/%@",kServerAdress, [[NSUserDefaults standardUserDefaults] stringForKey:@"usertoken"]]];
}

-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    if ([loadedData objectForKey:@"token"]) {
        NSLog(@"%@",[loadedData objectForKey:@"token"]);
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        [userDefaults setObject:[loadedData objectForKey:@"token"]
                         forKey:@"usertoken"];
        
        [userDefaults setBool:true forKey:@"logined"];
        
        [userDefaults synchronize];
    }
    
    if ([loadedData objectForKey:@"profile"]) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDictionary *profile = [loadedData objectForKey:@"profile"];
        NSLog(@"%@",loadedData);
        _userInfo = [[UserInfo alloc] init];
        [_userInfo setName:[profile objectForKey:@"name"]];
        [_userInfo setAvatar:[UIImage imageNamed:@"Tl_an.png"]];
        [_userInfo setTicketNumber:[NSString stringWithFormat:@"%@",[profile objectForKey:@"ticket"]]];
        
        NSString *normalDate = [[[profile objectForKey:@"enddate"] componentsSeparatedByString:@"T"] objectAtIndex:0];
        [_userInfo setTicketEndDate:[dateFormat dateFromString:normalDate]];
        
    }
    
    if ([loadedData objectForKey:@"data"]) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        
        NSMutableArray *leased = [[NSMutableArray alloc] init];
        for (NSDictionary *obj in [loadedData objectForKey:@"data"]) {
            BookOnHands *book1 = [[BookOnHands alloc] init];
            book1.title = [obj objectForKey:@"title"];
            book1.author = [obj objectForKey:@"name"];
            
            NSString *normalDate = [[[obj objectForKey:@"datee"] componentsSeparatedByString:@"T"] objectAtIndex:0];
            book1.leaseEndDate = [dateFormat dateFromString:normalDate];
            
            NSLog(@"%@",[obj objectForKey:@"pic"]);
            book1.cover = [UIImage imageNamed:[obj objectForKey:@"pic"]];
            
            [leased addObject:book1];
        }
        _booksOnHands = [NSArray arrayWithArray:leased];
        
        NSLog(@"%@",[loadedData objectForKey:@"data"]);
    }
    
    [self.delegate modelDidLoadDataFromRemoteSource:self];
    
//    NSLog(@"%@",[loadedData objectForKey:@"data"]);
//    
//    NSMutableArray *loadedBooks = [[NSMutableArray alloc] init];
//    for (NSDictionary *obj in [loadedData objectForKey:@"data"]) {
//        SearchResultBook *book1 = [[SearchResultBook alloc] init];
//        [book1 setTitle:[obj objectForKey:@"title"]];
//        [book1 setAuthor:[obj objectForKey:@"name"]];
//        [book1 setIsbn:[obj objectForKey:@"isbn"]];
//        [book1 setCover:[UIImage imageNamed:@"logo.png"]];
//        [loadedBooks addObject:book1];
//    }
//    
//    _searchResults = [NSArray arrayWithArray:loadedBooks];
    
//    [self.delegate modelDidLoadDataFromRemoteSource:self];
}

-(void)searchConnectionErrorWithText:(NSString *)error {
    
}

-(void)logout {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:@""
                     forKey:@"usertoken"];
    
    [userDefaults setBool:false forKey:@"logined"];
    
    [userDefaults synchronize];

}

@end
