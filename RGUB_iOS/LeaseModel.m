//
//  LeaseConnection.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "LeaseModel.h"

@implementation LeaseModel
-(void)leaseBookByIsbn:(NSString*)isbn {
    NSLog(@"isbn %@", isbn);
    
    SearchConnection *_connection = [[SearchConnection alloc] init];
    [_connection setDelegate:self];
    [_connection connect:[NSString stringWithFormat:@"%@/leaseset/%@/isbn/%@",kServerAdress, [[NSUserDefaults standardUserDefaults] stringForKey:@"usertoken"], isbn]];
}

-(void)searchConnectionFinishedLoadingData:(NSDictionary *)loadedData {
    NSLog(@"%@",loadedData);
    [self.delegate modelDidLoadDataFromRemoteSource:self];
}
@end
