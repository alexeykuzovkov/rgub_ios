//
//  EventsModel.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "EventsModel.h"

@implementation EventsModel

+ (id)sharedModel {
    static EventsModel *sharedModel = nil;
    @synchronized(self) {
        if (sharedModel == nil) {
            sharedModel = [[self alloc] init];
        }
    }
    return sharedModel;
}

-(id)init {
    if(self==[super init]) {
        _events = @[
                           [Event eventWithTitle:@"Открытие выставки Игоря Олейникова «L&Z»" andLink:@""],
                           [Event eventWithTitle:@"Министр природных ресурсов и экологии принял участие в научном стендапе в «День экологических знаний» в РГБМ" andLink:@""],
                           [Event eventWithTitle:@"Главные сюжеты европейского искусства: правила жизни, нормы и ценности" andLink:@""],
                           [Event eventWithTitle:@"Встреча с Владимиром Белобровым" andLink:@""]
                           ];
    }
    return self;
}

@end
