//
//  AfficheTableViewController.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AfficheModel.h"

@interface AfficheTableViewController : UITableViewController

@end
