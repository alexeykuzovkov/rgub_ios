//
//  SearchModel.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "Model.h"
#import "SearchResultBook.h"
#import "SearchConnection.h"

@interface SearchModel : Model <SearchConnectionDelegate>
{
    SearchConnection *connection;
}
-(id)initWithDelegate:(id)delegate;

-(void)searchByAuthor:(NSString*)author;
-(void)searchByTitle:(NSString*)title;
-(void)searchByISBN:(NSString*)isbn;

@property (nonatomic, strong) NSArray *searchResults;

@end
