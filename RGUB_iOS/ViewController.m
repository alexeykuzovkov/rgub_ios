//
//  ViewController.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "ViewController.h"
#import "EventsModel.h"
#import "Event.h"

@interface ViewController ()
{
    MTBBarcodeScanner *_scanner;
    UIView *_scannerPreview;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initScanner];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)unwindToMainMenu:(UIStoryboardSegue*)sender {
    
}


#pragma mark Scanner

-(void)initScanner {
    _scannerPreview = [[UIView alloc] initWithFrame:self.view.frame];
    [_scannerPreview setHidden:true];
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-80, self.view.frame.size.width, 80)];
    [cancelButton setTitle:@"Отменить" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelScanner) forControlEvents:UIControlEventTouchUpInside];
    [_scannerPreview addSubview:cancelButton];
    
    [self.view addSubview:_scannerPreview];
    _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_scannerPreview];
}

-(void)cancelScanner {
    [_scanner stopScanning];
    [_scannerPreview setHidden:true];
}
- (IBAction)scanISBN:(id)sender {
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            [_scannerPreview setHidden:false];
            NSError *error = nil;
            [_scanner startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                
                [_searchContextSegmentedControl setSelectedSegmentIndex:2];
                [_searchField setText:code.stringValue];
                
                [_scanner stopScanning];
                [_scannerPreview setHidden:true];
                
                [self performSegueWithIdentifier:@"searchResults" sender:nil];
            } error:&error];
            
        } else {
            // The user denied access to the camera
        }
    }];
}
-(void)scanForLogin {
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            [_scannerPreview setHidden:false];
            NSError *error = nil;
            [_scanner startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                
                [_scanner stopScanning];
                [_scannerPreview setHidden:true];
                
                [[LKModel sharedModel] setDelegate:self];
                [[LKModel sharedModel] loginWithCardNumber:code.stringValue];
                
            } error:&error];
            
        } else {
            // The user denied access to the camera
        }
    }];

}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [_searchField resignFirstResponder];
    }
}

#pragma mark tableviewdatasource

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"EventsCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Event *event = (Event*)[[[EventsModel sharedModel] events] objectAtIndex:indexPath.row];
    [(UILabel*)[cell.contentView viewWithTag:1] setText:event.title];
    return cell;

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[EventsModel sharedModel] events] count];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


#pragma mark tableviewdelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}


#pragma mark textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.searchField) {
        [textField resignFirstResponder];
        if (![textField.text isEqualToString:@""]) {
            [self performSegueWithIdentifier:@"searchResults" sender:nil];
        }
        return NO;
    }
    return YES;
}


#pragma mark navigation
-(void)showTicketInput {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Login"
                                                                              message: @"Введите номер читательского билета"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Номер читательского билета";
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * ticketfield = textfields[0];
        
        [[LKModel sharedModel] setDelegate:self];
        [[LKModel sharedModel] loginWithCardNumber:ticketfield.text];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Отменить" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(void)showLoginForm {
    NSLog(@"not logined");
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Необходимо войти"
                                                                   message:@"Выберите способ входа"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ввести номер билета" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self showTicketInput];
                                                          }];
    
    [alert addAction:defaultAction];
    
    UIAlertAction* defaultAction2 = [UIAlertAction actionWithTitle:@"Просканировать код билета" style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action) {
                                                               [self scanForLogin];
                                                           }];
    
    [alert addAction:defaultAction2];
    
    UIAlertAction* defaultAction3 = [UIAlertAction actionWithTitle:@"Отменить" style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction3];
    [self presentViewController:alert animated:YES completion:nil];
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"LK"]){
        if (![[LKModel sharedModel] isLogined]) {
            [self showLoginForm];
            return false;
        }
    }
    
    return true;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"searchResults"]) {
        BooksSearchResultsTableViewController *vc = (BooksSearchResultsTableViewController*)[segue destinationViewController];
        [vc setSearchQuery:_searchField.text];
        [vc setSearchType:(SearchType*)[_searchContextSegmentedControl selectedSegmentIndex]];
//        [self.searchField setText:@""];
    }
    
    
}

-(void)modelDidLoadDataFromRemoteSource:(Model*)model {
    if (model==[LKModel sharedModel] && [[LKModel sharedModel] isLogined]) {
        [self performSegueWithIdentifier:@"LK" sender:nil];
    }
    else {
        [self showLoginForm];
    }
}




@end
