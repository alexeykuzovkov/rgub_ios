//
//  BookOnHands.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "Model.h"
#import <UIKit/UIKit.h>

@interface BookOnHands : NSObject

@property (nonatomic, strong) UIImage *cover;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSDate *leaseEndDate;

@end
