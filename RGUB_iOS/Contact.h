//
//  Contact.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *contacts;
@end
