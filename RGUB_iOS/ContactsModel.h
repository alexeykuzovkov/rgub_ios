//
//  ContactsModel.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"
#import "Model.h"
@interface ContactsModel : Model

+ (id)sharedModel;

@property (nonatomic, strong, readonly) NSArray *contacts;

@end
