//
//  LKViewController.m
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "LKViewController.h"

#define kBookCoverTag   1
#define kBookTitleTag   2
#define kBookAuthorTag  3
#define kBookEndDateTag 4

@interface LKViewController ()
{
    NSDateFormatter *dateFormat;
}
@end

@implementation LKViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    
    [self fillUserInfo];
}

-(void)viewDidAppear:(BOOL)animated {
    [[LKModel sharedModel] setDelegate:self];
    [[LKModel sharedModel] loadRemoteProfile];
    [[LKModel sharedModel] loadLeaseInfo];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)prolongTicket:(id)sender {
}

- (IBAction)logout:(id)sender {
    [[LKModel sharedModel] logout];
    [self.navigationController popToRootViewControllerAnimated:true];
}

-(void)fillUserInfo {
    UserInfo *info = [[LKModel sharedModel] userInfo];
    NSLog(@"%@",info.name);
    [_userNameLabel setText:info.name];
    [_ticketNumberLabel setText:info.ticketNumber];
    [_endDateLabel setText:[NSString stringWithFormat:@"До %@",[dateFormat stringFromDate:info.ticketEndDate]]];
}

#pragma mark tableviewdatasource

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"LKBooksCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    BookOnHands *book = (BookOnHands*)[[[LKModel sharedModel] booksOnHands] objectAtIndex:indexPath.row];
    
    [(UIImageView*)[cell.contentView viewWithTag:kBookCoverTag] setImage:book.cover];
    

    [(UILabel*)[cell.contentView viewWithTag:kBookTitleTag] setText:book.title];
    [(UILabel*)[cell.contentView viewWithTag:kBookAuthorTag] setText:book.author];
    [(UILabel*)[cell.contentView viewWithTag:kBookEndDateTag] setText:[NSString stringWithFormat:@"До: %@",[dateFormat stringFromDate:book.leaseEndDate]]];
    return cell;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[LKModel sharedModel] booksOnHands] count];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


#pragma mark tableviewdelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

-(void)modelDidLoadDataFromRemoteSource:(Model *)model {
    [self fillUserInfo];
    [_tableView reloadData];
}


@end
