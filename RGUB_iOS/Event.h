//
//  Event.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 22.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject

+ (instancetype)eventWithTitle: (NSString *)title andLink:(NSString*)link;
-(id)initWithTitle:(NSString*)title andLink:(NSString*)link;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;

@end
