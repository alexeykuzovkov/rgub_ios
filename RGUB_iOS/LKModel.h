//
//  LKModel.h
//  RGUB_iOS
//
//  Created by Alex Kuzovkov on 23.04.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "Model.h"
#import "UserInfo.h"
#import "BookOnHands.h"

@interface LKModel : Model <SearchConnectionDelegate>
{
    SearchConnection *connection;
}

+ (id)sharedModel;

-(BOOL)isLogined;
-(void)logout;
-(void)loginWithCardNumber:(NSString*)cardNumber;

-(void)loadRemoteProfile;
-(void)loadLeaseInfo;

@property (nonatomic, readonly, strong) UserInfo *userInfo;
@property (nonatomic, readonly, strong) NSArray *booksOnHands;


@end
